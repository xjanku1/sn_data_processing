# Author: Kristyna Janku
import cv2
import os
import numpy as np
import json
import glob
from matplotlib import cm


def get_n_colors(n, colormap="gist_ncar"):
    # Function taken from SiamMOT implementation (vis_generator.py), author: Bing Shuai

    # Get n color samples from the colormap, derived from: https://stackoverflow.com/a/25730396/583620
    # gist_ncar is the default colormap as it appears to have the highest number of color transitions.
    # tab20 also seems like it would be a good option but it can only show a max of 20 distinct colors.
    # For more options see:
    # https://matplotlib.org/examples/color/colormaps_reference.html
    # and https://matplotlib.org/users/colormaps.html

    colors = cm.get_cmap(colormap)(np.linspace(0, 1, n))
    # Randomly shuffle the colors
    np.random.shuffle(colors)
    # Opencv expects bgr while cm returns rgb, so we swap to match the colormap (though it also works fine without)
    # Also multiply by 255 since cm returns values in the range [0, 1]
    colors = colors[:, (2, 1, 0)] * 255
    return colors


def get_int_coordinates_xyxy(coords):
    # Get integer coordinates from floats (possibly in string form)
    coords = [int(round(float(c))) for c in coords]
    # Change from x,y,w,h to x1,y1,x2,y2 (top left and bottom right corner)
    coords[2] += coords[0]
    coords[3] += coords[1]
    return coords


def visualize_results_sot(dataset_name, result_folder, network, show_image=False, hide_ids=False):
    dataset_folder = os.path.join(os.path.expanduser('~'), f'data/{dataset_name}')
    image_folder = os.path.join(dataset_folder, 'raw_data/test')
    ann_filename = os.path.join(dataset_folder, f'annotation/SOT/{dataset_name}.json')
    vis_folder = os.path.join(dataset_folder, f'visualization/{network}')
    if not os.path.exists(vis_folder):
        os.makedirs(vis_folder, exist_ok=True)

    num_colors = 50
    colors = get_n_colors(num_colors)

    res_dict = {}

    with open(ann_filename, 'r') as ann_file:
        ann_json = json.load(ann_file)

        for filename in os.listdir(result_folder):
            if filename.endswith('.txt'):
                seq = filename.split('.')[0]

                with open(os.path.join(result_folder, filename), 'r') as file:
                    obj = seq.split('_')[1]
                    coords = [get_int_coordinates_xyxy(line.split(',')) for line in file.readlines()]
                    images = ann_json[seq]['img_names']

                    for i in range(len(coords)):
                        if images[i] not in res_dict:
                            res_dict[images[i]] = {}
                        res_dict[images[i]][obj] = coords[i]

    for image_name in sorted(res_dict.keys()):
        image_path = os.path.join(image_folder, image_name)
        image = cv2.imread(image_path)
        for obj, coords in res_dict[image_name].items():
            color = colors[int(obj) % num_colors]
            cv2.rectangle(image, (coords[0], coords[1]), (coords[2], coords[3]), color)
            if not hide_ids:
                cv2.putText(image, obj, (coords[0] + 5, coords[1] + 30), cv2.FONT_HERSHEY_SIMPLEX, 1, color, thickness=2)
        video, frame = image_name.split('/')
        if show_image:
            cv2.imshow(f'Video: {video}, Frame: {frame}', image)
            cv2.waitKey(0)
        video_dir = os.path.join(vis_folder, video)
        if not os.path.exists(video_dir):
            os.mkdir(video_dir)
        cv2.imwrite(os.path.join(video_dir, frame), image)


def visualize_results_mot(dataset_name, result_folder, network, show_image=False, hide_ids=False):
    dataset_folder = os.path.join(os.path.expanduser('~'), f'data/{dataset_name}')
    res_file_path = glob.glob(os.path.join(result_folder, f'*.json'))[0]
    video = os.path.basename(res_file_path).split(".")[0]
    image_folder = os.path.join(dataset_folder, f'raw_data/test/{video}')
    vis_folder = os.path.join(dataset_folder, f'visualization/{network}')
    if not os.path.exists(vis_folder):
        os.makedirs(vis_folder, exist_ok=True)

    num_colors = 50
    colors = get_n_colors(num_colors)

    res_dict = {}

    with open(res_file_path, 'r') as res_file:
        predicted_video = [entity for entity in json.load(res_file)['entities'] if entity['id'] != -1]
        for entity in predicted_video:
            frame_index = entity['blob']['frame_idx']
            if frame_index not in res_dict:
                res_dict[frame_index] = {}
            res_dict[frame_index][entity['id']] = get_int_coordinates_xyxy(entity['bb'])

    for image_name in sorted(os.listdir(image_folder)):
        if image_name.endswith('.png'):
            image_path = os.path.join(image_folder, image_name)
            image = cv2.imread(image_path)
            frame_number = (image_name.split("t")[1]).split(".")[0]

            if int(frame_number) in res_dict:
                for obj, coords in res_dict[int(frame_number)].items():
                    color = colors[obj % num_colors]
                    cv2.rectangle(image, (coords[0], coords[1]), (coords[2], coords[3]), color)
                    if not hide_ids:
                        cv2.putText(image, str(obj), (coords[0] + 5, coords[1] + 30), cv2.FONT_HERSHEY_SIMPLEX, 1, color, thickness=2)

                if show_image:
                    cv2.imshow(f'Video: {video}, Frame: {frame_number}', image)
                    cv2.waitKey(0)

                video_dir = os.path.join(vis_folder, video)
                if not os.path.exists(video_dir):
                    os.mkdir(video_dir)
                cv2.imwrite(os.path.join(video_dir, image_name), image)


if __name__ == '__main__':
    dataset_name = 'Fluo-N2DH-SIM+'

    # Siam RPN++
    network = 'SiamRPN++'
    model_name = 'checkpoint_e19'
    res_folder = os.path.join(os.path.expanduser('~'),
                              'siamese_networks/pysot/experiments/siamrpn_r50_l234_dwxcorr/results',
                              dataset_name,
                              model_name)

    visualize_results_sot(dataset_name, res_folder, network, show_image=False)

    # Siam R-CNN
    network = 'Siam R-CNN'
    model_name = 'ThreeStageTracker_SIM_vid02/model-1500000_0.06_0.3_0.3_0.1_0.9_7.0'
    res_folder = os.path.join(os.path.expanduser('~'),
                              'siamese_networks/SiamR-CNN_TF2/tracking/tracking_data/results',
                              dataset_name,
                              model_name)

    visualize_results_sot(dataset_name, res_folder, network, show_image=False)

    # SiamMOT
    network = 'SiamMOT'
    model_name = 'DLA-34-FPN_box_EMM_Fluo-N2DH-SIM+_train_video_02'
    res_folder = os.path.join(os.path.expanduser('~'),
                              'siamese_networks/siam-mot/results',
                              model_name)

    visualize_results_mot(dataset_name, res_folder, network, show_image=False)


    # dataset_name = 'BF-C2DL-MuSC'
    #
    # # Siam RPN++
    # network = 'SiamRPN++'
    # model_name = 'checkpoint_e19'
    # res_folder = os.path.join(os.path.expanduser('~'),
    #                           'siamese_networks/pysot/experiments/siamrpn_r50_l234_dwxcorr/results',
    #                           dataset_name,
    #                           model_name)
    #
    # visualize_results_sot(dataset_name, res_folder, network, show_image=False)
    #
    # # Siam R-CNN
    # network = 'Siam R-CNN'
    # model_name = 'ThreeStageTracker_MuSC_vid02/model-1850000_0.06_0.3_0.3_0.1_0.9_3.0'
    # res_folder = os.path.join(os.path.expanduser('~'),
    #                           'siamese_networks/SiamR-CNN_TF2/tracking/tracking_data/results',
    #                           dataset_name,
    #                           model_name)
    #
    # visualize_results_sot(dataset_name, res_folder, network, show_image=False)
    #
    # # SiamMOT
    # network = 'SiamMOT'
    # model_name = 'DLA-34-FPN_box_EMM_BF-C2DL-MuSC_train_video_02'
    # res_folder = os.path.join(os.path.expanduser('~'),
    #                           'siamese_networks/siam-mot/results',
    #                           model_name)
    #
    # visualize_results_mot(dataset_name, res_folder, network, show_image=False)
    #
    #
    # dataset_name = 'BF-C2DL-HSC'
    #
    # # Siam RPN++
    # network = 'SiamRPN++'
    # model_name = 'checkpoint_e19'
    # res_folder = os.path.join(os.path.expanduser('~'),
    #                           'siamese_networks/pysot/experiments/siamrpn_r50_l234_dwxcorr/results',
    #                           dataset_name,
    #                           model_name)
    #
    # visualize_results_sot(dataset_name, res_folder, network, show_image=False, hide_ids=True)
    #
    # Siam R-CNN
    # network = 'Siam R-CNN'
    # model_name = 'ThreeStageTracker_HSC_vid01/model-1950000_0.06_0.3_0.3_0.1_0.9_7.0'
    # res_folder = os.path.join(os.path.expanduser('~'),
    #                           'siamese_networks/SiamR-CNN_TF2/tracking/tracking_data/results',
    #                           dataset_name,
    #                           model_name)
    #
    # visualize_results_sot(dataset_name, res_folder, network, show_image=False, hide_ids=True)
    #
    # # SiamMOT
    # network = 'SiamMOT'
    # model_name = 'DLA-34-FPN_box_EMM_BF-C2DL-HSC_train_video_01'
    # res_folder = os.path.join(os.path.expanduser('~'),
    #                           'siamese_networks/siam-mot/results',
    #                           model_name)
    #
    # visualize_results_mot(dataset_name, res_folder, network, show_image=False, hide_ids=True)
