import re
import matplotlib.pyplot as plt
from os import path


def visualize_loss(log_paths, regex, steps='Iterations', warmup_steps=25, mot=False):
    fig, ax = plt.subplots()

    for lr, log_path in log_paths.items():
        with open(log_path, 'r') as logfile:
            lines = ' '.join([line.rstrip('\n') for line in logfile.readlines()])
            matches = re.findall(regex, lines)
            if mot:
                matches = matches[::2]
            iters, losses = zip(*matches)
            iters = [int(x) for x in iters][warmup_steps:]
            losses = [float(x) for x in losses][warmup_steps:]

        ax.plot(iters, losses, label=lr, linewidth=0.5)

    ax.legend()
    plt.xlabel(steps)
    plt.ylabel('Loss')
    plt.show()


if __name__ == '__main__':
    dataset = 'Fluo-N2DH-SIM+'

    # # SiamMOT
    #
    # root = path.join(path.expanduser('~'), 'siamese_networks/siam-mot/models')
    # if dataset == 'Fluo-N2DH-SIM+':
    #     log_paths = {'LR=0.00125 WD=0.00005': path.join(root,
    #         'DLA-34-FPN_box_EMM_Fluo-N2DH-SIM+_train_image_02_SIM_vid02_lr00125_decay00005/nohup_siammot_sim_lr00125_decay00005.out')}
    # elif dataset == 'BF-C2DL-MuSC':
    #     log_paths = {'LR=0.00125 WD=0.00005': path.join(root,
    #         'DLA-34-FPN_box_EMM_BF-C2DL-MuSC_train_video_02_MuSC_video_02_67ms_lr00125_decay00005_prep3/nohup_siammot_musc_prep3_67ms.out')}
    # else:
    #     log_paths = {'LR=0.00125 WD=0.00005': path.join(root,
    #         'DLA-34-FPN_box_EMM_BF-C2DL-HSC_train_video_01_HSC_video_01_34ms_lr00125_decay00005/nohup_siammot_hsc.out')}
    # regex = 'iter: (\d+)  loss: (\d\.\d+)'
    # # Logger outputs each 20 iterations - 500 warmup iterations / 20 = 25 logs
    # visualize_loss(log_paths, regex, steps='Iterations', warmup_steps=25, mot=True)

    # Siam R-CNN

    root = path.join(path.expanduser('~'), 'siamese_networks/SiamR-CNN_TF2/train_log/siamrcnn')
    if dataset == 'Fluo-N2DH-SIM+':
        log_paths = {'Fluo-N2DH-SIM+': path.join(root, 'SIM_vid02/nohup_rcnn_sim.out')}
    elif dataset == 'BF-C2DL-MuSC':
        log_paths = {'BF-C2DL-MuSC': path.join(root, 'MuSC_vid02/nohup_rcnn_musc.out')}
    else:
        log_paths = {'BF-C2DL-HSC': path.join(root, 'HSC_vid01/nohup_rcnn_hsc.out')}
    regex = 'Start Epoch (\d+).*?total_cost: (\d\.\d+)'
    visualize_loss(log_paths, regex, steps='Epochs', warmup_steps=2)
