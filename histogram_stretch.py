# Author: Kristyna Janku

import cv2
import numpy as np


def find_percentiles_16bit(image_16bit, percent=0):
    # Calculate standard histogram
    hist = cv2.calcHist([image_16bit], [0], None, [65536], [0, 65536])

    num_pixels = image_16bit.size
    low_percentile = 65535
    high_percentile = 0
    # Convert percent to decimal representation
    percent = percent / 100

    # Compute percentiles while making cumulative histogram
    for i in range(1, hist.size):
        hist[i] += hist[i-1]
        if low_percentile == 65535 and hist[i] >= num_pixels * percent:
            low_percentile = i
        elif high_percentile == 0 and hist[i] >= num_pixels * (1 - percent):
            high_percentile = i

    return low_percentile, high_percentile


def histogram_stretch_16to8bit(image_16bit, low, high):
    # Set outliers to low/high
    image_16bit = np.where(image_16bit < low, low, image_16bit)
    image_16bit = np.where(image_16bit > high, high, image_16bit)

    # Transform values to range 0-255, low becomes 0, high becomes 255, other values are linearly scaled between them
    img_scaled = (image_16bit - low) * (255 / (high - low))

    # Round and convert float values to 8-bit unsigned integers
    img_scaled = np.uint8(np.rint(img_scaled))

    return img_scaled
