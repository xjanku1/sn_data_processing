# Author: Kristyna Janku

import glob
import cv2
from os import path


def create_video(img_dir):
    img_paths = sorted(glob.glob(path.join(img_dir, '*.png')))

    if len(img_paths) == 0:
        print("No PNG images found at given path")
    else:
        img = cv2.imread(path.join(img_dir, img_paths[0]))
        height, width, _ = img.shape
        size = (width, height)

        out = cv2.VideoWriter(f'{path.join(img_dir, path.basename(img_dir))}.mp4',
                              cv2.VideoWriter_fourcc(*'mp4v'), 30, size)

        out.write(img)

        for filename in img_paths[1:]:
            img = cv2.imread(path.join(img_dir, filename))
            out.write(img)

        out.release()


if __name__ == '__main__':
    img_directory = path.join(path.expanduser('~'), 'data/BF-C2DL-MuSC/raw_data/train/02')
    create_video(img_directory)
