import os
import json
import numpy as np
import cv2
from collections import defaultdict

from visualize_results import get_int_coordinates_xyxy


def check_bbox_contains_marker(bbox, row, col):
    return bbox[0] <= min(col) and bbox[1] <= min(row) and bbox[2] >= max(col) and bbox[3] >= max(row)


def track_fractions(dataset_name, result_folder, tracker_type="SOT", continuous=True):
    ann_path = os.path.join(os.path.expanduser('~'), f'data/{dataset_name}/annotation/SOT/{dataset_name}.json')
    complete = 0
    fractions = []
    if tracker_type == "MOT":
        gt_to_mot_ids = {}

    with open(ann_path, 'r') as ann_file:
        annotations = json.load(ann_file)

        # Load MOT results for whole video
        if tracker_type == "MOT":
            vid = list(annotations.keys())[0].split('_')[0]
            with open(os.path.join(result_folder, f'{vid}.json'), 'r') as res_file:
                predicted_video = [entity for entity in json.load(res_file)['entities'] if entity['id'] != -1]

        for seq, values in annotations.items():
            vid = seq.split('_')[0]
            obj = seq.split('_')[1]
            images = values['img_names']

            # Load SOT results for sequence
            if tracker_type == "SOT":
                with open(os.path.join(result_folder, f'{seq}.txt'), 'r') as res_file:
                    predicted = [get_int_coordinates_xyxy(line.split(','))
                                 for line in res_file.readlines()]

            correct_positions = [0]
            all_positions = len(images)
            tracks_path = os.path.join(os.path.expanduser('~'), f'data/{dataset_name}/raw_data/test/{vid}_GT/TRA')

            for i in range(len(images)):
                frame_number = (images[i].split("t")[1]).split(".")[0]
                track_filename = f'man_track{frame_number}.tif'
                track_image = cv2.imread(os.path.join(tracks_path, track_filename), cv2.IMREAD_ANYDEPTH)

                # Get indices of pixels belonging to the marker of current object from tracking annotations
                row, col = np.where(track_image == int(obj))
                bbox = []

                if tracker_type == "MOT":
                    if obj not in gt_to_mot_ids:
                        # Find matching rectangles to current object id marker
                        matches = [entity for entity in predicted_video
                                   if entity['blob']['frame_idx'] == int(frame_number) and
                                   entity['id'] not in gt_to_mot_ids.values() and
                                   check_bbox_contains_marker(get_int_coordinates_xyxy(entity['bb']), row, col)]
                        if len(matches) > 0:
                            # Sort matches by bounding box area and select the smallest one
                            matches.sort(key=lambda x: x['bb'][2] * x['bb'][3])
                            gt_to_mot_ids[obj] = matches[0]['id']
                            bbox = get_int_coordinates_xyxy(matches[0]['bb'])

                    else:
                        predicted = [entity for entity in predicted_video
                                     if entity['blob']['frame_idx'] == int(frame_number) and
                                        entity['id'] == gt_to_mot_ids[obj]]
                        if len(predicted) > 0:
                            bbox = get_int_coordinates_xyxy(predicted[0]['bb'])

                else:
                    bbox = predicted[i]

                # Check if predicted bounding box contains the whole marker of current object from tracking annotations
                if len(bbox) > 0 and check_bbox_contains_marker(bbox, row, col):
                    correct_positions[-1] += 1

                # Start new counter if we only count continuous correct tracks
                elif continuous and correct_positions[-1] != 0:
                    correct_positions.append(0)

            fractions.append(max(correct_positions) / all_positions)
            if not continuous and max(correct_positions) == all_positions:
                complete += 1

    if continuous and tracker_type == 'MOT':
        print(f'GT to MOT ID mapping: {gt_to_mot_ids}')

    mean = np.mean(fractions)
    print(f'Track fractions {"continuous" if continuous else "any"}: {mean:.3f}')
    if not continuous:
        print(f'Complete tracks: {complete / len(annotations):.3f}')


def eval_configuration_errors(gt_dict, est_dict):
    # Array for results [TP, FP, FN, MT, MO]
    total_stats = np.zeros(5)

    for frame in gt_dict:
        frame_stats = np.zeros(5)

        for gt, (row, col) in gt_dict[frame].items():
            matches = [e for e, bbox in est_dict[frame].items() if check_bbox_contains_marker(bbox, row, col)]
            if len(matches) == 0:
                # FN
                frame_stats[2] += 1
            elif len(matches) == 1:
                # TP
                frame_stats[0] += 1
            else:
                # MT
                frame_stats[3] += (len(matches) - 1)

        for est, bbox in est_dict[frame].items():
            matches = [gt for gt, (row, col) in gt_dict[frame].items() if check_bbox_contains_marker(bbox, row, col)]
            if len(matches) == 0:
                # FP
                frame_stats[1] += 1
            elif len(matches) > 1:
                # MO
                frame_stats[4] += (len(matches) - 1)

        num_gt = len(gt_dict[frame])
        total_stats += frame_stats / num_gt

    return total_stats


def compute_configuration_errors(dataset_name, result_folder, tracker_type):
    ann_path = os.path.join(os.path.expanduser('~'), f'data/{dataset_name}/annotation/SOT/{dataset_name}.json')

    # Dictionary for frame estimates with bounding boxes
    est_dict = defaultdict(dict)
    # Dictionary for frame ground truth with tracking marker coordinates
    gt_dict = defaultdict(dict)

    with open(ann_path, 'r') as ann_file:
        annotations = json.load(ann_file)

        # Load MOT results for whole video
        if tracker_type == "MOT":
            vid = list(annotations.keys())[0].split('_')[0]
            with open(os.path.join(result_folder, f'{vid}.json'), 'r') as res_file:
                predicted = [entity for entity in json.load(res_file)['entities'] if entity['id'] != -1]
                for entity in predicted:
                    est_dict[entity['blob']['frame_idx']][entity['id']] = get_int_coordinates_xyxy(entity['bb'])

        for seq, values in annotations.items():
            vid = seq.split('_')[0]
            obj = seq.split('_')[1]
            images = values['img_names']

            # Load SOT results for sequence
            if tracker_type == "SOT":
                with open(os.path.join(result_folder, f'{seq}.txt'), 'r') as res_file:
                    predicted = [get_int_coordinates_xyxy(line.split(','))
                                 for line in res_file.readlines()]

            tracks_path = os.path.join(os.path.expanduser('~'), f'data/{dataset_name}/raw_data/test/{vid}_GT/TRA')

            for i in range(len(images)):
                frame_number = (images[i].split("t")[1]).split(".")[0]
                track_filename = f'man_track{frame_number}.tif'
                track_image = cv2.imread(os.path.join(tracks_path, track_filename), cv2.IMREAD_ANYDEPTH)

                # Get indices of pixels belonging to the marker of current object from tracking annotations
                row, col = np.where(track_image == int(obj))

                gt_dict[int(frame_number)][int(obj)] = (row, col)

                if tracker_type == "SOT":
                    est_dict[int(frame_number)][int(obj)] = predicted[i]

    total_stats = eval_configuration_errors(gt_dict, est_dict)

    num_frames = len(gt_dict)
    print(f'TP: {total_stats[0] / num_frames:.3f}')
    print(f'FP: {total_stats[1] / num_frames:.3f}')
    print(f'FN: {total_stats[2] / num_frames:.3f}')
    print(f'MT: {total_stats[3] / num_frames:.3f}')
    print(f'MO: {total_stats[4] / num_frames:.3f}')

    return est_dict


# Adjusted function from SiamMOT implementation
def bbs_iou(bboxes_1, bboxes_2):
    """
    Compute iou matrix between two lists of bounding boxes
    """

    if len(bboxes_1) == 0 or len(bboxes_2) == 0:
        return np.zeros((len(bboxes_1), len(bboxes_2)))

    box_xyxy_1 = np.array(bboxes_1)
    box_xyxy_2 = np.array(bboxes_2)

    # Compute the area of union regions
    area1 = (box_xyxy_1[:, 2] - box_xyxy_1[:, 0] + 1) * (box_xyxy_1[:, 3] - box_xyxy_1[:, 1] + 1)
    area2 = (box_xyxy_2[:, 2] - box_xyxy_2[:, 0] + 1) * (box_xyxy_2[:, 3] - box_xyxy_2[:, 1] + 1)

    # Compute left-top and right-bottom corners of the intersection region
    lt = np.maximum(box_xyxy_1[:, np.newaxis, :2], box_xyxy_2[:, :2])  # [N,M,2]
    rb = np.minimum(box_xyxy_1[:, np.newaxis, 2:], box_xyxy_2[:, 2:])  # [N,M,2]

    # Compute width and height of the intersection region and its area
    wh = (rb - lt).clip(min=0)  # [N,M,2]
    inter = wh[:, :, 0] * wh[:, :, 1]  # [N,M]

    iou = inter / (area1[:, np.newaxis] + area2 - inter)

    return iou


# Adjusted function from SiamMOT implementation
def greedy_matching(iou_matrix, iou_thresh=0.01):
    """
        Do the greedy matching across detections and ground truth annotations
        Returns the ious for every detection and ground truth matched pair
    """
    (num_det, num_gt) = iou_matrix.shape
    gt_ious = np.zeros(num_gt)

    if iou_matrix.size > 0:
        for i in range(num_gt):
            max_iou = np.amax(iou_matrix)
            if max_iou >= iou_thresh:
                # Get ground truth id for the maximum IoU
                _id = np.where(iou_matrix == max_iou)[1][0]
                gt_ious[_id] = max_iou
                # Set the IoUs for matched ground truth to 0, so it cannot be matched again
                iou_matrix[:, _id] = 0
    return gt_ious


def compute_detection_ious(dataset_name, est_dict):
    ann_path = os.path.join(os.path.expanduser('~'), f'data/{dataset_name}/annotation/SOT/{dataset_name}.json')

    # Dictionary for ground truth bounding boxes
    gt_dict = defaultdict(dict)

    total_avg_iou = 0

    with open(ann_path, 'r') as ann_file:
        annotations = json.load(ann_file)

        for seq, values in annotations.items():
            obj = seq.split('_')[1]
            images = values['img_names']
            bboxes = values['gt_rect']

            for i in range(len(images)):
                frame_number = (images[i].split("t")[1]).split(".")[0]
                gt_dict[int(frame_number)][int(obj)] = get_int_coordinates_xyxy(bboxes[i])

    for frame in gt_dict.keys():
        frame_gt = list(gt_dict[frame].values())
        detections = list(est_dict[frame].values())
        iou_matrix = bbs_iou(detections, frame_gt)
        matched_ious = greedy_matching(iou_matrix)
        frame_avg_iou = np.sum(matched_ious) / len(frame_gt)
        total_avg_iou += frame_avg_iou

    print(f'Avg IoU: {total_avg_iou / len(gt_dict):.3f}')
    

def compute_measures(network, dataset_name, result_folder, tracker_type):
    print()
    print(network)
    track_fractions(dataset_name, result_folder, tracker_type, continuous=True)
    track_fractions(dataset_name, result_folder, tracker_type, continuous=False)
    est_dict = compute_configuration_errors(dataset_name, result_folder, tracker_type)
    compute_detection_ious(dataset_name, est_dict)


if __name__ == '__main__':
    dataset_name = 'Fluo-N2DH-SIM+'
    print(f'Dataset: {dataset_name}')

    # Siam RPN++
    network = 'SiamRPN++'
    tracker_type = 'SOT'
    model_name = 'checkpoint_e19'
    result_folder = os.path.join(os.path.expanduser('~'),
                                 'siamese_networks/pysot/experiments/siamrpn_r50_l234_dwxcorr/results',
                                 dataset_name,
                                 model_name)

    compute_measures(network, dataset_name, result_folder, tracker_type)

    # Siam R-CNN
    network = 'Siam R-CNN'
    tracker_type = 'SOT'
    model_name = 'ThreeStageTracker_SIM_vid02/model-1500000_0.06_0.3_0.3_0.1_0.9_7.0'
    result_folder = os.path.join(os.path.expanduser('~'),
                                 'siamese_networks/SiamR-CNN_TF2/tracking/tracking_data/results',
                                 dataset_name,
                                 model_name)

    compute_measures(network, dataset_name, result_folder, tracker_type)

    # SiamMOT
    network = 'SiamMOT'
    tracker_type = 'MOT'
    model_name = 'DLA-34-FPN_box_EMM_Fluo-N2DH-SIM+_train_video_02'
    result_folder = os.path.join(os.path.expanduser('~'),
                                 'siamese_networks/siam-mot/results',
                                 model_name)

    compute_measures(network, dataset_name, result_folder, tracker_type)


    # dataset_name = 'BF-C2DL-MuSC'
    # print(f'Dataset: {dataset_name}')
    #
    # # Siam RPN++
    # network = 'SiamRPN++'
    # tracker_type = 'SOT'
    # model_name = 'checkpoint_e19'
    # result_folder = os.path.join(os.path.expanduser('~'),
    #                              'siamese_networks/pysot/experiments/siamrpn_r50_l234_dwxcorr/results',
    #                              dataset_name,
    #                              model_name)
    #
    # compute_measures(network, dataset_name, result_folder, tracker_type)
    #
    # # Siam R-CNN
    # network = 'Siam R-CNN'
    # tracker_type = 'SOT'
    # model_name = 'ThreeStageTracker_MuSC_vid02/model-1850000_0.06_0.3_0.3_0.1_0.9_3.0'
    # result_folder = os.path.join(os.path.expanduser('~'),
    #                              'siamese_networks/SiamR-CNN_TF2/tracking/tracking_data/results',
    #                              dataset_name,
    #                              model_name)
    #
    # compute_measures(network, dataset_name, result_folder, tracker_type)
    #
    # # SiamMOT
    # network = 'SiamMOT'
    # tracker_type = 'MOT'
    # model_name = 'DLA-34-FPN_box_EMM_BF-C2DL-MuSC_train_video_02'
    # result_folder = os.path.join(os.path.expanduser('~'),
    #                              'siamese_networks/siam-mot/results',
    #                              model_name)
    #
    # compute_measures(network, dataset_name, result_folder, tracker_type)
    #
    #
    # dataset_name = 'BF-C2DL-HSC'
    # print(f'Dataset: {dataset_name}')
    #
    # # Siam RPN++
    # network = 'SiamRPN++'
    # tracker_type = 'SOT'
    # model_name = 'checkpoint_e19'
    # result_folder = os.path.join(os.path.expanduser('~'),
    #                              'siamese_networks/pysot/experiments/siamrpn_r50_l234_dwxcorr/results',
    #                              dataset_name,
    #                              model_name)
    #
    # compute_measures(network, dataset_name, result_folder, tracker_type)
    #
    # Siam R-CNN
    # network = 'Siam R-CNN'
    # tracker_type = 'SOT'
    # model_name = 'ThreeStageTracker_HSC_vid01/model-1950000_0.06_0.3_0.3_0.1_0.9_7.0'
    # result_folder = os.path.join(os.path.expanduser('~'),
    #                              'siamese_networks/SiamR-CNN_TF2/tracking/tracking_data/results',
    #                              dataset_name,
    #                              model_name)
    #
    # compute_measures(network, dataset_name, result_folder, tracker_type)
    #
    # # SiamMOT
    # network = 'SiamMOT'
    # tracker_type = 'MOT'
    # model_name = 'DLA-34-FPN_box_EMM_BF-C2DL-HSC_train_video_01'
    # result_folder = os.path.join(os.path.expanduser('~'),
    #                              'siamese_networks/siam-mot/results',
    #                              model_name)
    #
    # compute_measures(network, dataset_name, result_folder, tracker_type)
