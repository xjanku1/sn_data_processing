# Author: Kristyna Janku

from os import listdir, path, makedirs
import cv2
import numpy as np
from skimage import morphology

import histogram_stretch


def compute_histogram_percentiles(root_data_path, videos, percent):
    avg_low = 0
    avg_high = 0
    num_images = 0

    for video in videos:
        video_path = path.join(root_data_path, video)
        for image in listdir(video_path):
            num_images += 1
            image_16bit = cv2.imread(path.join(video_path, image), cv2.IMREAD_ANYDEPTH)
            low, high = histogram_stretch.find_percentiles_16bit(image_16bit, percent)
            avg_low += low
            avg_high += high

    avg_low = round(avg_low / num_images)
    avg_high = round(avg_high / num_images)

    return avg_low, avg_high


def preprocess_image_all(image, h):
    kernel_3 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    open_close = cv2.morphologyEx(cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel_3),
                                  cv2.MORPH_CLOSE, kernel_3)

    im_h = cv2.add(open_close, h)
    h_min = (morphology.reconstruction(im_h, open_close, 'erosion')).astype(np.uint8)

    im_h = cv2.subtract(h_min, h)
    h_max = (morphology.reconstruction(im_h, h_min)).astype(np.uint8)

    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(16, 16))
    image_clahe = clahe.apply(h_max)

    kernel_5 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    open_close = cv2.morphologyEx(cv2.morphologyEx(image_clahe, cv2.MORPH_OPEN, kernel_5),
                                  cv2.MORPH_CLOSE, kernel_5)

    image = cv2.bilateralFilter(open_close, 7, 30, 7)

    return image


def preprocess_image_morph(image, h):
    kernel_3 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    open_close = cv2.morphologyEx(cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel_3),
                                  cv2.MORPH_CLOSE, kernel_3)

    im_h = cv2.add(open_close, h)
    h_min = (morphology.reconstruction(im_h, open_close, 'erosion')).astype(np.uint8)

    im_h = cv2.subtract(h_min, h)
    h_max = (morphology.reconstruction(im_h, h_min)).astype(np.uint8)

    return h_max


def preprocess_images(root_data_path, videos, percent=0.1, suffix='pre', prep='all', h=10):
    low, high = compute_histogram_percentiles(root_data_path, videos, percent)

    for video in videos:
        video_path = path.join(root_data_path, video)
        result_path = path.join(root_data_path, f'{video}_{suffix}')
        if not path.isdir(result_path):
            makedirs(result_path)

        for filename in listdir(video_path):
            # Load 16-bit TIFF image, convert it to 8-bit and perform percentile stretch
            image_16bit = cv2.imread(path.join(video_path, filename), cv2.IMREAD_ANYDEPTH)
            image_8bit = histogram_stretch.histogram_stretch_16to8bit(image_16bit, low, high)

            if prep == 'all':
                image_8bit = preprocess_image_all(image_8bit, h)
            elif prep == 'morph':
                image_8bit = preprocess_image_morph(image_8bit, h)

            cv2.imwrite(path.join(result_path, f'{filename.split(".")[0]}.png'), image_8bit)


if __name__ == '__main__':
    dataset = 'BF-C2DL-MuSC'
    subset = 'test'
    root_data_folder = path.join(path.expanduser('~'), f'data/{dataset}/raw_data/{subset}')
    video_folders = ['01_orig']

    # The percentile stretching parameter
    # percent = 0.1 (used for all datasets)

    # The h-min and h-max transformations parameter
    # h = 18 for SIM sequence 02 (train)
    # h = 10 for SIM sequence 01 (test)
    # h = 3 for MuSC sequence 02 (train)
    # h = 20 for MuSC sequence 01 (test)
    # not used for HSC

    # Preprocessing options:
    # 'all' - morphological transformations (open-close, h-min, h-max), CLAHE, open-close, bilateral filter; used for Fluo-N2DH-SIM+
    # 'morph' - only morphological transformations (open-close, h-min, h-max); used for BF-C2DL-MuSC
    # 'none' - used for BF-C2DL-HSC
    preprocess_images(root_data_folder, video_folders, percent=0.1, suffix='pre_morph_h20', prep='morph', h=20)

